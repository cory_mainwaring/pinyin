package pinyin

import (
	"errors"
	"regexp"
	"strconv"
	"strings"
)

type Pinyin string

func (p *Pinyin) String() string {
	return (string)(*p)
}

func (p *Pinyin) Contains(term string) bool {
	search := StripPinyin(New(term))
	containing := StripPinyin(*p)
	return strings.Contains(containing, search)
}

func (p *Pinyin) Match(term string) bool {
	search := New(term)
	return search == *p
}

func (p *Pinyin) Normalize() {
	*p = (Pinyin)(NormalizePinyin((string)(*p)))
}

func New(pinyin string) Pinyin {
	P := (Pinyin)(pinyin)
	P.Normalize()
	return P
}

func StripPinyin(p Pinyin) string {
	result := p.String()
	for key, val := range pinyinTable {
		for _, pinVal := range val {
			result = strings.Replace(result, pinVal, key, -1)
		}
	}
	return result
}

var pinyinTable = map[string][]string{
	"a": []string{"a", "ā", "á", "ǎ", "à"},
	"e": []string{"e", "ē", "é", "ě", "è"},
	"i": []string{"i", "ī", "í", "ǐ", "ì"},
	"o": []string{"o", "ō", "ó", "ǒ", "ò"},
	"u": []string{"u", "ū", "ú", "ǔ", "ù"},
	"v": []string{"ü", "ǖ", "ǘ", "ǚ", "ǜ"},
	"A": []string{"A", "Ā", "Á", "Ǎ", "À"},
	"E": []string{"E", "Ē", "É", "Ě", "È"},
	"I": []string{"I", "Ī", "Í", "Ǐ", "Ì"},
	"O": []string{"O", "Ō", "Ó", "Ǒ", "Ò"},
	"U": []string{"U", "Ū", "Ú", "Ǔ", "Ù"},
	"V": []string{"Ü", "Ǖ", "Ǘ", "Ǚ", "Ǜ"},
}

var NoToneNumberError = errors.New("No tone number")
var ToneOutOfRangeError = errors.New("Tone out of range")
var TooManySyllablesError = errors.New("Have too many syllables to handle")
var NoToneVowelError = errors.New("No tone vowel")

func SeparateToneNumber(syl string) (Result string, ToneNumber int, Error error) {
	Result = syl
	ToneNumber = -1

	ToneIndex := strings.IndexAny(syl, "0123456789")
	if ToneIndex < 0 {
		Error = NoToneNumberError
		return
	}
	Result = syl[:ToneIndex]

	CurIndex := ToneIndex
	for CurIndex != len(syl)-1 {
		CurIndex++
		NextIndex := strings.IndexAny(syl[CurIndex:], "0123456789")
		if NextIndex != 0 {
			Error = TooManySyllablesError
			return
		}
	}

	// NOTE(cory): Should have nothing but 0-9 based on earlier tests
	ToneNumber, _ = strconv.Atoi(syl[ToneIndex:])

	if ToneNumber < 1 || ToneNumber > 5 {
		Error = ToneOutOfRangeError
		return
	}
	return
}

func VowelIndex(s string) int {
	return strings.IndexAny(s, "aeiouvAEIOUV")
}

func CountVowels(Syl string) int {
	Count := 0
	CurIndex := 0
	NextIndex := VowelIndex(Syl)
	for NextIndex > -1 {
		Count++
		CurIndex += NextIndex + 1
		NextIndex = VowelIndex(Syl[CurIndex:])
	}
	return Count
}

func FindToneVowel(Syl string) string {
	if CountVowels(Syl) == 1 {
		Index := VowelIndex(Syl)
		return Syl[Index : Index+1]
	}
	var vowelCaptures = []struct {
		test    string
		capture string
	}{
		{"IU", "U"},
		{"iu", "u"},
		{"A", "A"},
		{"a", "a"},
		{"E", "E"},
		{"e", "e"},
		{"O", "O"},
		{"o", "o"},
		{"I", "I"},
		{"i", "i"},
	}
	for _, v := range vowelCaptures {
		if strings.Contains(Syl, v.test) {
			return v.capture
		}
	}
	return ""
}

func NormalizeTone(in string) (string, error) {

	Syllable, ToneNumber, Error := SeparateToneNumber(in)
	if Error != nil {
		return in, Error
	}
    if Syllable == "r" && ToneNumber == 5 {
        return "r", nil
    }
	ToneVowel := FindToneVowel(Syllable)
	if ToneVowel == "" {
        return in, NoToneVowelError
	}

	return strings.Replace(Syllable, ToneVowel, pinyinTable[ToneVowel][ToneNumber%5], -1), nil
}

var SyllableMatcher = regexp.MustCompile(`[BPMFDTNLGKHJQXZHCHSHRZCSYWAOEIVUbpmfdtnlgkhjqxzhchshrzcsywaoeivu]+?[1-5]`)

func NormalizePinyin(in string) string {
	Words := strings.Fields(in)
	for i, _ := range Words {
		for SyllableMatcher.MatchString(Words[i]) {
			Syllable := SyllableMatcher.FindString(Words[i])
            ToneVowel, err := NormalizeTone(Syllable);
            if err != nil {
                break
            }
			Words[i] = strings.Replace(Words[i], Syllable, ToneVowel, 1)
		}
	}

	return strings.Join(Words, " ")
}

var PinyinRegex = regexp.MustCompile(`[a-zA-Z]+?[1-5]`)

func Possible(in string) bool {
    if PinyinRegex.MatchString(in) {
        return true
    }
    return false
}
