package pinyin

import (
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Equal(t *testing.T, Input, Expected, Result interface{}) {
	if Expected != Result {
		t.Errorf("On %q: Expected %q but got %q.", Input, Expected, Result)
	}
}
func IsNil(t *testing.T, Value interface{}) {
	if Value != nil {
		t.Errorf("Wasn't Nil: %s", Value)
	}
}

/*
func TestToneMatch(t *testing.T) {
	var Input string = "pin1"
	var Expected string = "pīn"
	var Result string
	Result = NormalizeTone(Input)
	if Result != Expected {
		t.Errorf("Expected %q but got %q. Fail.", Expected, Result)
	}
}
*/

type stn_return struct {
	Syl   string
	Tone  int
	Error error
}

func SeperateToneNumberTester(t *testing.T, Input string, Expected stn_return) {
	Result := NewSTN(SeparateToneNumber(Input))
	Equal(t, Input, Expected.Syl, Result.Syl)
	Equal(t, Input, Expected.Tone, Result.Tone)
	Equal(t, Input, Expected.Error, Result.Error)
}

func NewSTN(Syl string, Tone int, Error error) stn_return {
	return stn_return{Syl, Tone, Error}
}

func TestSeperateToneNumber(t *testing.T) {
	Convey("Given a valid syllable with tone number", t, func() {
		input := "pin1"
		retSyl, retTone, retErr := SeparateToneNumber(input)
		Convey("Returned syllable should be 'pin'", func() {
			So(retSyl, ShouldEqual, "pin")
		})
		Convey("Returned tone should be 1", func() {
			So(retTone, ShouldEqual, 1)
		})
		Convey("Returned error should be nil", func() {
			So(retErr, ShouldBeNil)
		})
	})
	Convey("Tone Out of Range Tests", t, func() {
		Convey("When Range is high", func() {
			input := "pin15"
			_, _, retErr := SeparateToneNumber(input)
			Convey("Should receive a ToneOutOfRangeError", func() {
				So(retErr, ShouldEqual, ToneOutOfRangeError)
			})
		})
		Convey("When Range is extremely high", func() {
			input := "pin1500"
			_, _, retErr := SeparateToneNumber(input)
			Convey("Should receive a ToneOutOfRangeError", func() {
				So(retErr, ShouldEqual, ToneOutOfRangeError)
			})
		})
		Convey("When Range is low", func() {
			input := "pin0"
			_, _, retErr := SeparateToneNumber(input)
			Convey("Should receive a ToneOutOfRangeError", func() {
				So(retErr, ShouldEqual, ToneOutOfRangeError)
			})
		})
	})
	Convey("When syllable has anything after the tone number", t, func() {
		input := "pin1yin2"
		_, _, retErr := SeparateToneNumber(input)
		Convey("Should receive a TooManySyllablesError", func() {
			So(retErr, ShouldEqual, TooManySyllablesError)
		})
	})
	SeperateToneNumberTester(t, "pin1", NewSTN("pin", 1, nil))
	SeperateToneNumberTester(t, "pin", NewSTN("pin", -1, NoToneNumberError))
	SeperateToneNumberTester(t, "1", NewSTN("", 1, nil))
}

func TestFindToneVowel(t *testing.T) {
	tests := []struct {
		input  string
		output string
	}{
		// NOTE(cory): single vowel combos
		{"bo", "o"},
		{"ba", "a"},
		{"bi", "i"},
		{"bu", "u"},
		{"bv", "v"},

		// NOTE(cory): two vowel combos
		{"bao", "a"},
		{"pie", "e"},
		{"dui", "i"}, // also tests ui special case
		{"qiong", "o"},
		{"jiu", "u"}, // also tests iu special case
		{"nve", "e"},

		// NOTE(cory): three vowel combos
		{"miao", "a"},
		{"zhuai", "a"},

		// NOTE(cory): no tone vowels
		{"", ""},
		{"b", ""},
		{"buu", ""},
	}

	Convey("Syllables with vowels should return the right vowels", t, func() {
		for _, v := range tests {
			Convey(fmt.Sprintf("\"%s\" should return \"%s\"", v.input, v.output), func() {
				So(FindToneVowel(v.input), ShouldEqual, v.output)
			})
		}
	})
}

func TestCountVowels(t *testing.T) {
	tests := []struct {
		input  string
		output int
	}{
		{"", 0},
		{"b", 0},
		{"ba", 1},
		{"be", 1},
		{"bi", 1},
		{"bo", 1},
		{"bu", 1},
		{"bv", 1},
		{"bae", 2},
		{"baaa", 3},
		{"baeiouv", 6},
		{"aeiouvb", 6},
		{"baeiouvb", 6},
	}

	for _, v := range tests {
		Convey(fmt.Sprintf("Vowel Count of \"%s\" is %d", v.input, v.output), t, func() {
			So(CountVowels(v.input), ShouldEqual, v.output)
		})
	}
}

func TestVowelIndex(t *testing.T) {
	tests := []struct {
		input  string
		output int
	}{
		{"schlimy", 4},
		{"aardvark", 0},
		{"shi", 2},
	}
	for _, v := range tests {
		Convey(fmt.Sprintf("Vowel Index of \"%s\" is %d", v.input, v.output), t, func() {
			So(VowelIndex(v.input), ShouldEqual, v.output)
		})
	}
}

func TestNormalizeTone(t *testing.T) {
	tests := []struct {
		input  string
		output string
        err     error
	}{
		{"pin1", "pīn", nil},
		{"pin1yin1", "pin1yin1", TooManySyllablesError},
		{"pn1", "pn1", NoToneVowelError},
		{"pn", "pn", NoToneNumberError},
	}
	for _, v := range tests {
		Convey(fmt.Sprintf("Normalized tone for \"%s\" should be \"%s\"", v.input, v.output), t, func() {
            ToneVowel, Error := NormalizeTone(v.input)
			So(ToneVowel, ShouldEqual, v.output)
			So(Error, ShouldEqual, v.err)
		})
	}
}

func TestNormalizePinyin(t *testing.T) {
	tests := []struct {
		input  string
		output string
	}{
		{"pin1 yin2", "pīn yín"},
		{"xu3duo4", "xǔduò"},
		{"XU3DUO4", "XǓDUÒ"},
		{"PAI3 DUI4", "PǍI DUÌ"},
		{"JIE3 JV4", "JIĚ JǛ"},
		{"3D", "3D"},
		{"3D zhu3yin1", "3D zhǔyīn"},
		{"3D-zhu3yin1", "3D-zhǔyīn"},
		{"pin1yin1xu2duo1", "pīnyīnxúduō"},
		{"pīnyīnxúduō", "pīnyīnxúduō"},
		{"heart4house4", "heàrthousè"},
		{"tou2 gu1 r5", "tóu gū r"},
	}

	Convey("Given unnormalized pinyin, output correct pinyin", t, func() {
		for _, v := range tests {
			Convey(fmt.Sprintf("\"%s\" -> \"%s\"", v.input, v.output), func() {
				So(NormalizePinyin(v.input), ShouldEqual, v.output)
			})
		}
	})
}

func TestPinyinCreation(t *testing.T) {
	tests := []struct {
		input  string
		output Pinyin
	}{
		{"pin1 yin2", Pinyin("pīn yín")},
		{"xu3duo4", Pinyin("xǔduò")},
		{"XU3DUO4", Pinyin("XǓDUÒ")},
		{"PAI3 DUI4", Pinyin("PǍI DUÌ")},
		{"JIE3 JV4", Pinyin("JIĚ JǛ")},
		{"3D", Pinyin("3D")},
		{"3D zhu3yin1", Pinyin("3D zhǔyīn")},
		{"3D-zhu3yin1", Pinyin("3D-zhǔyīn")},
		{"pin1yin1xu2duo1", Pinyin("pīnyīnxúduō")},
		{"pīnyīnxúduō", Pinyin("pīnyīnxúduō")},
		{"heart4house4", Pinyin("heàrthousè")},
	}
	Convey("Given any string, create a normalized Pinyin", t, func() {
		for _, v := range tests {
			Convey(fmt.Sprintf("\"%s\" -> \"%q\"", v.input, v.output), func() {
				So(New(v.input), ShouldEqual, v.output)
			})
		}
	})
}

func TestContains(t *testing.T) {
	ContainingPinyin := New("Bùshì dōngfēng yādǎo xīfēng, jiùshì xīfēng yādǎo dōngfēng")

	var tests = []struct {
		test        string
		normalized  Pinyin
		shouldMatch bool
	}{
		{"dong1feng1", Pinyin("dōngfēng"), true},
		{"dōngfēng", Pinyin("dōngfēng"), true},
		{"bushi", Pinyin("bushi"), false},
		{"Bushi", Pinyin("Bushi"), true},
		{"yadao", Pinyin("yadao"), true},
	}

	Convey(fmt.Sprintf("Containing pinyin is %q", ContainingPinyin.String()), t, func() {

		for _, v := range tests {

			Convey(fmt.Sprintf("Test string is %q", v.test), func() {
				normalizedSearch := New(v.test)

				Convey(fmt.Sprintf("Normalized to Pinyin->%q", v.normalized), func() {

					So(normalizedSearch, ShouldEqual, v.normalized)

					ShouldMatchText := " not"
					if v.shouldMatch {
						ShouldMatchText = ""
					}
					Convey(fmt.Sprintf("It should"+ShouldMatchText+" match the containing pinyin"), func() {

						So(ContainingPinyin.Contains(normalizedSearch.String()), ShouldEqual, v.shouldMatch)
					})

				})

			})
		}

	})

}

func TestMatch(t *testing.T) {
	tests := []struct {
		matcher    Pinyin
		matches    []string
		nonMatches []string
	}{
		{New("dong1feng1"), []string{"dong1feng1", "dōngfeng1"}, []string{"dongfeng", "dong", "feng", "dong1"}},
	}

	for _, v := range tests {
		Convey(fmt.Sprintf("Given pinyin of %q", v.matcher), t, func() {

			for _, match := range v.matches {
				Convey(fmt.Sprintf("Match %q", match), func() {
					So(v.matcher.Match(match), ShouldBeTrue)
				})
			}

			for _, nonMatch := range v.nonMatches {
				Convey(fmt.Sprintf("Do *not* match %q", nonMatch), func() {
					So(v.matcher.Match(nonMatch), ShouldBeFalse)
				})
			}

		})
	}
}
